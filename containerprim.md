# Linux container Primitives 
* these can used outside of containers and inside them
* if a namespace or cgroup has root it can control child processes 
## cgroups
* Organize Processes and add quotas 
* a series of subsystems mostly around resource control
* each subsystem is completely idenendent and can be nested ex a proess can be inside a group inside another subsystem
* cgroups are mounted in /sys/fs/cgroup
* one pid can exist in multiple cgroups
* writing to the filesystem is using an interface to edit settings in the cgroups

### non container users
* can be used to control resource limits for specific process
* monitor and ogranize processes
* docs are in Documentation/cgroups-v1 in the linux kernal source tree

## Namespaces 
* isolation for resources
* changes to resources are only visble to the namespace
* exisitng namespaces
  * uid
  * network
  * filesystem
  * pid
* docker run uses this for networking along with bridges
* visible in /proc/$$/ns/* 
* files are simlinks to the namespace
* you can't manipulate it from the filesystem it has to be done via syscalls 
* namespaces can't be empty they have to have something keeping them open or the kernel will delete them
* to enter namespace you need to use a syscall 

## images layers and unionFS 
* images are faked filesystems
* common in virt and container runtimes
* unionFS is a set of layers of diffs of a base images
* are writes treated as just adding a layer
* deleting file just removes it from a layer it still takes space
* saves a ton spaces since common files exist once so the same base image isn't stored more than once only the difference between each instance of that image

### overlayFS
* joins two dictories to form a unionFS upper dir and lower dir
* existing files are moved to upperdir when being opened with write access
* overlay will keep a copy in the lower when removed from a lower dir 
* upperdirs can have multiple lower dirs 
* can be examined with /proc/mounts and /proc/$$/
* uses linux virtual systems to allow for overlay's to have different filesystems
* cannot restore layers by default but docker does it for you 
* docker does have device mapper for managing block storage 
* zfs and btrfs use snapshots which are block level
* overlayFS is specific to docker

## OCI  and runtimes 
* a runtime is a software tool that runs containers ex docker cri-o rkt containerd....
* docker was the first won that people used 
* OCI = part of linux foundation to standardize parts of containers like storage 
* most use runc for running except systemd-nspawn 
* containers are bundles 
  * json doc + filesystem
  * filesystem can be a unionFS 
  * json doc describes
    * cgroups
    * namespaces
    * capbilites
    * selinux and app armour details 
* hooks can run
  * before start
  * after start
  * after stop
* hooks run sequentially 
* docker generates bundles w/o hooks
* docker will let you specify a runtime
