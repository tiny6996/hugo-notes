# Can we fix email encryption (STARTTLS not PGP)

## why this matters
* we are not getting rid of it so we might as well fix it 
* there are people snooping on plaintext email
* STARTTLS Everywhere

## Demo of why plain text email 
* mail in the box -> striptls proxy -> gmail
* the proxy found a bunch of garbage when sent with ssl on after the servers established that they both support ssl
* when SSL was striped te whole message was plain text
* there are ISPs that do TLS stripping and possibly some nation states

## why is it like this 
* there are really 3 browsers 
* there are ton of MTA's bunch of open source ones and propertairy services like office.com and gmail.com
* email either sends or doesn't but there is no standard throwing an error if a message is not encrypted
  * no fancy errors like self signed certs in browsers 

### What we want 
* TLS
* documented way to verify
* a way to fail if not encrypted 
* all three done in a way that doesn't break things and is easy

### solution
* use DANE and DNSSEC 
* uses dns records to verify who you are 
* DNSSEC is not common
* bleeding edge problem first movers get security and miss mail
* MTA-STS 
  * uses DNS and https
  * stores settings mta-sts.txt in .well-known and caches them for 90 days
  * still downgrade if your DNS server is reliable 
  * easier to setup than ding 
  * not in postfix or exim
* https://dl.eff.org/starttls-everywhere/poliy.json
* TLSRPT will send a report if any TLS errors occcur works with DANE and MTA-STS
* put up DANE records
* lookup LETS encrpyt certs plus DANE 
* look at writing checks for TLSRPT
* healthchecks.io will send an email if your mailserver isn't working 
* hardenize.com will give reports for email
* sencys.io
