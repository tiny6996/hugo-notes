# Get it Back how to back data in postgres

## do it the hardway by hand 
* don't just copy the DB on disk since postgres uses shared ram buffers so changes are not written directly to disk
* they can keep acid complicance and use shared buffers because postgres uses a transaction log which can be used to recover the database
* Transaction log can be used to create a replica by playing the transaction log to a different postgres server 
* base backup is the database at the start of the transaction log 
* to create base backup
  * wal_lavel = replica
  * archive_mode on 
  * archive_command = cp -r pg data directory 
* pg_start_ backup()
  * set start point
  * checkpoint 
  * return
* pg_stop_backup()
  * waits for last archive to complete 
  * cleanup
* use pg_basebackup will do this for you 
* you can send the write ahead log straight to an archive to create point in time recovery

## projects that do all this for me 
* pgBackRest
* Barman
* pgHoard
* Wale-E
* pghoard
* WAL-G

### look for 
* cloud storage integration or generic s3/swift compatbility
* encryption 
* management interface
* how fast are they
* retention

## local backups with pg_dump
* series of sql statements that can be used to rebuild a database
* works with normal postgres conn
* selective, single datase or just special tables methods above get the whole database cluster or nothing 
* works out of the box
* output is text (readable by human)
* used to be the only way to get data from one version of postgres to another
* compressable 
* can be edited 
* pg_dumpall == pgdumpall --globals + pg_dump(-C)
* no way to use transaction logs 
* hard to optimize performance
* restores slower (it is running a shitton of sql statements)
  * basebackup is similar to a copy 
* there is a custom format in pg_dump that is semi human readable
  * pg_dump --format=c for custom or pg_dump --format=d for directory
  * can be used for parrellel restore with pg_restore
  * can generate parts of the data from the backup like the create statements or restore data only or schema only
* uses less space than dumping the VM
* might not be atomtic since stuff could be in shared buffers basically the same as a file copy and recovery is like coming back power loss