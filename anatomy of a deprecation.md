# Anatomy of a deprecation
* beta in 2015
* public launch in april 16
* root CA trusted by all browse
* uses acme to validate a domain and issue/renew certs
* can validate using 
  * put a file on a webserver HTTP-01
  * TXT records DNS-01
  * TLS-SNI-01 puta tokein in a cert on your server 

## what is TLS-SNI-01
* ACME generates a token
* ACME looks up the ip for a domain
* connects to that ip on port 443
* expects a cert with the correct hostname
* the domain should have self signed cert with the token
* is supposed to be better than http validation since you some webservers don't to allow file uploads
* spoofing a cert is supposed to be harder than uploading a file 

## what is wrong with TLS-SNI
* some hosting providers allow uploading certs for domains you don't controll
* spotted by Detectify in Jan 2018

## response
* confirmed and disabled tls-sni-01 within 90 minutes
* later redeployed a way to only allow renews with TLS-SNI-01
* could this have been fixed using a different subdomain ex acme.token.example.com
* made a plan to create TLS-ALPN(application layer ) with IETF (internet engineering task force)
  * fails without sending a cert
  * creates strong assocation to hostname 
  * still allows for regular certs to be used



## Issues switching to TLS-ALPN
* nginx and apache don't support it 
* there are a lot of existing installs using the depricadted validation
  * changed default to http-01 and could fallback to TLS-SNI to see who actually need it 
* LTS releases were not updating to newer releases of certbot

## results 
* no huge influx of salty users but the certs are not all expired yet

### what could have gone better
* communicate early and often 
  * OS packagers could have had more time to upgrade
  * more webadmins could change there configs 
* send emails sooner
* send emails with domains
* send emails with documentation to fix it
* send test batch
* reduce dependenacies if you can 
* maybe wait till the software is more stable before pushing it LTSs' like debian or ubuntu LTS
  * new software has more bugs and more addtions that are not going to get software 
  * maybe use sandboxed formats like snap/flatpak or use your repo/ppa

### what went well
* people were nice to noobs
  * people were not shamed for asking basic questions
* fourm community helped all the people
* people actually read the email
* responded quickly to disclosure 

### lessons for users
* use an email address so you know you are renewing or methods are being depricated
* make sure you are patching your shit 
* make sure port 80 is open and HSTS and redirects are enabled 
