# 7 mistakes you are making in mysql
* if you're a comedian and not one is laughing you become a notification speaker
* slides T https://slideshare.com/davidmstokes
* elephantdolphin.blogspot.com

## they have backups but they don't test them
* use multiple methods 
* use mysqldump/mysqlpump (use pump)
* LVM snapshots 
* repliaction to another instance
* backup binary logs 
* backup stored procedures and views if you use them (they are not backuped by default)
* backup configs and keys 
* make sure stuff is stored in multiple places 
* what is your retention policy and test it 
* keep copies of warrantys 

### Be able to restore the following 
* make sure your backup can do it 
  * make other people setup and do it 
  * make a depth chart 
* entire sever 
* schema
* table 
* row 
* murphy was an optimist 

## Patch your shit 
* you get new features, bug fixes, and security fixes 
* backup server before and after updates 

## Monitor
* first law of databases Nobody ever bitches their database is too fast
* hardware
* all transactions and configs like indexs and connections
* does your paycheck of your boss depend a relible database? then make sure you What the fuck it is doing 
* use tools like MYSQL workbench (Doesn't work with maria is diverged too far Percona still works )
* clean up your user tables 
  * MYSQL looks for the most vague match first ex user@% matches before user@192.168.1.%
* avoid full table scans (were each row has to be read)
  * use indicies to conditions to avoid this 
  * don't index every column
  * updating indcies is expsenive 
  * mo indicies mo problems each index is a factorial increase
  * use compound indexes 
  * use sys schema for performance stas like unused indexes
    * make sure not to run right after reboot and double check before deleting 
* use histogram for some columns 
``` sql
ANALYZE TABLE customer UPDATE HISTOGRAM ON col with 1024 buckes 
```
* set index to inivisible and rerun queries before deleting 

## learn to optimize queries
* learn to explain and read query plans 
* buy a copy HIGH Performance mysql or the Certification GUide
2. learn to use Visual EXPLAIN from mysql workbench
3. Read DAILY slow query log 
4. optimize the most frequent queries first 

## user JSON columns 
* works for every RMDBS besides SQL server
* helps for data that has some nosql like problems (OEM data)
* MYSQL document store which uses mysql JSON columns to create document database 
* json_table can temporialy change a json columns to regular column
* you can gerate regular columns from JSON data in mysql


