# mesh networks with openvpn
* came from openvpn and bind9
  * uses bind9 to use address servers to multiple clients
  * openvpn scales poorly 
  * troubleshooting openvpn is a pita
* slides on https://jrs-s.net/presentations/
* blog https://jrs-s.net

## properties for a mesh networks
* makes sure clients can't route to eachother if they for seperate clients
* make sure clients can't reach out to you only you can reach into them
* make sure you have human address names
* works on 100+ nodes

## why openvpn sucks
* frequent disconnects
* buggly slow reconnects
  * 8 seconds or more to reconnect between two exeons
* security issues
* router interface
* complicated creds 
  * username + pasword + an x509 cert with a ca cert + keys
* netgear hogs port 1194
* is peer to peer all the time so can't be paired with dhcp since it can't broadcast
* runs in userspace on not linux
  * still better than openvpn on android
* no standard port number

## why wireguard is awesome
* way better code
  * linus said good things and didn't roast him
* insanely fast reconnections
* secure by default
* much easier config 
* 6 hours while watch kids to setup...thats about how long it takes you to set it up in pfsense with a web gui
* 1% the Sloc of opvenvpn or ipsec
  * easier to audit 
* stabilitiy (no issues with a connection...yet)
* can't be configured insecure and already has primitive to deal with newer versions
* runs in kernelspace on linux
* network manager exists...no comment

## what sucks about WG 
* no DHCP
  * ips don't have to be dynamic...
  * can be automated
* no offical windows client...yet and should be using the windows vpn interface
  * mostly a linux systems and all windows machines are virtualized and can use virt manager to remote in...lucky bastard

## wgadmin
* automatically id 
* autogen new keys
* add dns
* assign ip
* wg-addpeer
  * parse live wgconfig
  * find first available ip in sub and assign it
  * generate new keypair
  * update both live config and config files
  * dump new client configs to stdout
* will automate DNS eventually 
* aviailbe on github https://github.com/jimsalterjrs/wg-admin is super duper alpha 
