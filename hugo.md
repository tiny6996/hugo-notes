# hugo notes
* install with a snap with ```sudo snap install hugo```
  * apt version is very out of date check to see if this works with ubuntu server
* install apache2 and git with ```sudo apt isntall git apache2```
* cd into new directory 
* create a new site with ```hugo new site sitename```
* add a theme with by changing into the site directory and running ``` git add submodule add gitrepo.git themes/theme_name```
* you can also just download a theme in put it in a themes folder
* to set your theme(template) add a line in config.toml ``` theme = theme_name ```
* create a post by running ```hugo new dir/file.md``` or copy another post and edit it
  * MAKE SURE TO RUN IS IN THE SITES ROOT FOLDER IT CREATES POSTS RELATIVE TO WERE THEY ARE RUN
* see if the site is running by running ```hugo server -D``` this will watch for changes in hugo dir and render changes live on localhost
* in the archetypes folder contains a file called default.md that is a template that is renders a markdown file when you run hugo new
* to create a new archetype create a file called archetype_name.md
* [to use a template run hugo new archetype_name/filename.md](https://gohugo.io/content-management/archetypes/)
* data directory stores json,yaml,toml that can be load into html pages
* layouts are html templates outline how stuff is the content folder is rendered 
* static is extra java script css images vector graphics etc
* content is organized like a file hierarchy ex an article called fart in posts would be at https://exmple.com/posts/fart
* naming a file _index.md file return it when you go that path in a url 
  * if you want a page to show in https://example.com/about create a the file in content/about/_index.md
  * there is no are no files in a directory it returns a 404
* to create a new page do hugo create /folder/filename.md
* the _index.md header will show in the page 
* a single page is a file in a folder 
* a list page is a folder 
* _index.md front matter is always shown 
* front matter is the stuff inbetween +++ +++ or --- ---

### examples
* about
  * _index.md (list page with content and front matter)

* about
  * _index.md (list page with front matter only)
  * mathias.md (single page)
  * jie.md (single) 

* layouts are html files located in the _default folder that can inject content to all pages
* [variables](https://gohugo.io/variables)
* variables are accessed by placeing text in {{ }} similar to jinja in python
* {{ define header }} and {{ define main }} will add in the theme